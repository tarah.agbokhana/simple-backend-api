package com.watermyplants.services;

import com.watermyplants.JavaApplicationTests;
import com.watermyplants.exceptions.ResourceNotFoundException;
import com.watermyplants.controllers.models.User;
import com.watermyplants.repositories.UserRepository;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;


import java.util.*;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest(//webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
    classes = JavaApplicationTests.class,
    properties = {"command.line.runner.enabled=false"})

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class UserServiceImplTest {

  @Autowired
  private UserService userService;

  @Autowired
  private UserRepository userRepository;

  @MockBean
  List<User> userList;

  @MockBean
  private UserRepository userrepos;

  @MockBean
  private HelperFunctions helperFunctions;

  @Before
  public void setUp() throws Exception {
    userList = new ArrayList<>();

    User u1 = new User("test",
                       "dogs",
                       "4349818441");
    User u2 = new User("test1",
                       "password",
                       "5555555555");
  }

  @After
  public void tearDown() throws Exception {
  }

  @Test
  public void test_findUserById() {
    Mockito.when(userrepos.findById(1L))
           .thenReturn(Optional.of(userList.get(0)));

    assertEquals("testadmin", userService.findUserById(1).getUsername());
  }
  @Test(expected = ResourceNotFoundException.class)
  public void test_findUserByIdNotFound()
  {
    Mockito.when(userrepos.findById(10000L))
           .thenThrow(ResourceNotFoundException.class);

    assertEquals("testadmin",
                 userService.findUserById(10000)
                            .getUsername());
  }
}
