package com.watermyplants.repositories;

import com.watermyplants.controllers.models.Plant;
import org.springframework.data.repository.CrudRepository;

public interface PlantRepository extends CrudRepository<Plant,Long> {
}
