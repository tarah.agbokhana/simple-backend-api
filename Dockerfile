FROM tomcat:9.0.54-jdk11
COPY target/*.war /usr/local/tomcat/webapps/springboot-tomcat.war
EXPOSE 8080

